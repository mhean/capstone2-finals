@extends('templates.template')
@section('title','Add Student')
@section('content')
<h1 class="text-center py-5">Add Student</h1>
@if(Session::has("message"))
		<h4 class="text-center py-1">{{Session::get('message')}}</h4>
@endif
<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/admin/addstudent" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="date">Exam Date:</label>
					<input type="date" name="date" class="form-control">
				</div>
				
				<div class="form-group">
					<label for="firstName">Firstname:</label>
					<input type="text" name="firstName" class="form-control">
				</div>
				<div class="form-group">
					<label for="lastName">Lastname:</label>
					<input type="text" name="lastName" class="form-control">
				</div>
				<div class="form-group">
					<label for="batch">Batch:</label>
					<select name="batch_id" class="form-control">
						@foreach($batches as $batch)
						<option value="{{$batch->id}}">{{$batch->name}}</option>
						@endforeach
					</select>
					
				</div>
				<div class="form-group">
					<label for="subject">Subject:</label>
					<select name="subject_id" class="form-control">
						@foreach($subjects as $subject)
						<option value="{{$subject->id}}">{{$subject->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="score">Exam Score:</label>
					<input type="number" name="score" class="form-control">
				</div>
				<div class="form-group">
					<label for="marks">Mark:</label>
					<select name="mark_id" class="form-control">
						@foreach($marks as $mark)
						<option value="{{$mark->id}}">{{$mark->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="role_id">Role:</label>
					<select name="role_id" class="form-control">
						@foreach($roles as $role)
						<option value="{{$role->id}}">{{$role->name}}</option>
						@endforeach
					</select>
					
				</div>
				<button type="submit" class="btn btn-success">Add</button>
			</form>
		</div>
	</div>
</div>
@endsection
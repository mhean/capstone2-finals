@extends('templates.template')
@section('title','Request Form')
@section('content')
<h1 class="text-center py-5">Request Form</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/studentrequest" method="POST">
				@csrf
				<div class="form-group">
					<label for="student_id">Student Name:</label>
					<select name="student_id" class="form-control">
						@foreach($students as $student)
						<option value="{{$student->id}}">{{$student->firstName}} {{$student->lastName}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="batch_id">Batch:</label>
					<select name="batch_id" class="form-control">
						@foreach($batches as $batch)
						<option value="{{$batch->id}}">{{$batch->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="subject_id">Subject:</label>
					<select name="subject_id" class="form-control">
						@foreach($subjects as $subject)
						<option value="{{$subject->id}}">{{$subject->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input type="email" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label for="request">Comment/Request:</label>
					<textarea type="text" name="request" class="form-control" placeholder="Please type your request here"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">submit</button>
			</form>
		</div>
	</div>	
</div>
@endsection
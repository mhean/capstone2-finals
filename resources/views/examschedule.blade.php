@extends('templates.template')
@section('title','Schedule')
@section('content')


<div class="exam-schedule">
<h1 class="text-center py-5">Examination Schedule</h1>
	@if(Session::has("message"))
		<h4 class="text-center alert alert-success">{{Session::get('message')}}</h4>
	@endif
	

<!-- ADD SCHEDULE MODAL -->
<div class="modal fade" id="addschedule">
		  <div class="modal-dialog " role="document" >
		    <div class="modal-content" style="background-color: #dfe6e9">
		      <div class="modal-header" style="background-color: #b2bec3">
		        <h5 class="modal-title">Add New Schedule</h5>

		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		        <form action="/examschedule" method="POST">
				@csrf				
			      <div class="modal-body">
					<div class="form-group">
						<label>Date of Examination:</label>
						<input type="date" name="date" class="form-control">
					</div>				
					<div class="form-group">
						<label for="subject">Subject:</label>
						<select name="subject_id" class="form-control">
							@foreach($subjects as $subject)
							<option value="{{$subject->id}}">{{$subject->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="batch">Batch:</label>
						<select name="batch_id" class="form-control">
							@foreach($batches as $batch)
								<option value="{{$batch->id}}">{{$batch->name}}</option>
							@endforeach
						</select>
					</div>	
					<div class="form-group">
						<label>Room No:</label>
						<input type="number" name="roomNo" class="form-control">
					</div>
					<div class="form-group">
						<label>Start Time:</label>
						<input type="time" name="startTime" class="form-control">
					</div>
					<div class="form-group">
						<label>End Time:</label>
						<input type="time" name="endTime" class="form-control">
					</div>
					<button type="submit" class="btn btn-success">Add Schedule</button>			
			      </div>
			      <!-- <div class="offset-lg-1"> -->
					<!-- <a data-toggle="modal" data-target="#addstudent" class="btn btn-primary">Add New </a>	 -->
				<!-- </div> -->
				</form>
		    </div>
		 </div>
</div>
<!-- END ADD SCHEDULE MODAL -->
	

<div class="container">
	@auth
	@if(Auth::user()->role_id == 1)
	<div>
		<a data-toggle="modal" data-target="#addschedule" class="btn btn-add">Add Schedule</a>			
	</div>
	@endauth
	@endif

	<div class="row schedule-form">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped ">
				<thead>
					<tr class="thead">
						<th>@sortablelink('Examination Date')</th>
						<th>@sortablelink('Batch')</th>
						<th>@sortablelink('Subject')</th>
						<th>@sortablelink('Room No')</th>
						<th>@sortablelink('Start Time')</th>
						<th>@sortablelink('End Time')</th>
						<th>@sortablelink('Created At')</th>
							@auth
							@if(Auth::user()->role_id == 1)
						<th>Action</th>						
							@endif
							@endauth
						
					</tr>
				</thead>
				<tbody>
					@if($exam_scheds->count())
					@foreach($exam_scheds as $exam_sched)
					<tr class="tbody">
						<td>{{$exam_sched->date}}</td>
						<td>{{$exam_sched->batch->name}}</td>
						<td>{{$exam_sched->subject->name}}</td>
						<td>{{$exam_sched->roomNo}}</td>						
						<td>{{$exam_sched->startTime}}</td>
						<td>{{$exam_sched->endTime}}</td>	
						<td>{{$exam_sched->created_at->diffForHumans()}}</td>					
						<td>
							@auth
							@if(Auth::user()->role_id == 1)						
							<a href="/examschedule/{{$exam_sched->id}}" class="btn btn-info form-control"><i class="fa fa-edit" style="font-size:20px;color:#fff;"></i></a>
							
							<form class="delete_form" action="/examschedule/{{$exam_sched->id}}" method="POST">
								@csrf
								{{method_field("DELETE")}}
								<input type="hidden" name="_method" value="DELETE">
								<button type="submit" class="btn btn-danger form-control"><i class="fa fa-remove" style="font-size:20px;color:#fff;"></i></button>
							</form>
							@endif
							@endauth
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>

		</div>		
			
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function(){
		$('.delete_form').on('submit',function(){
			if(confirm("Are you sure you want to delete it?"))
			{
				return true;
			}else{
				return false;
			}
		});
	});
</script>
@endsection


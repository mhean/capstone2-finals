@extends('templates.template')
@section('title','Edit Schedule')
@section('content')

<div style="background-image: url(/images/students.jpg); background-size:cover; background-attachment: fixed; color: #0984e3; font-weight: bolder; text-shadow: 1px 1px #fff;">
<h1 class="text-center py-5">Edit Schedule</h1>
	
<div class="container editschedule">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/examschedule/{{$exam_sched->id}}" method="POST">
				@csrf							      
					@csrf
					{{method_field("PATCH")}}
					<div class="form-group">
						<label for="date">Date of Examination:</label>
						<input type="date" name="date" class="form-control" value="{{$exam_sched->date}}">
					</div>				
					<div class="form-group">
						<label for="subject_id">Subject:</label>
						<select name="subject_id" class="form-control" >
							@foreach($subjects as $subject)
							<option value="{{$subject->id}}" {{$subject->id == $exam_sched->subject_id ? "selected" : ""}}>{{$subject->name}}</option>
							@endforeach
						</select>					
					</div>
						<div class="form-group">
						<label for="batch">Batch:</label>
						<select name="batch_id" class="form-control">
							@foreach($batches as $batch)
								<option value="{{$batch->id}}">{{$batch->name}}</option>
							@endforeach
						</select>
					</div>	
					<div class="form-group">
						<label for="roomNo">Room Number:</label>
						<input type="number" name="roomNo" class="form-control" value="{{$exam_sched->roomNo}}">
					</div>
					<div class="form-group">
						<label for="startTime">Start Time:</label>
						<input type="time" name="startTime" class="form-control" value="{{$exam_sched->startTime}}">
					</div>
					<div class="form-group">
						<label for="endTime">End Time:</label>
						<input type="time" name="endTime" class="form-control" value="{{$exam_sched->endTime}}">
					</div>
					
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				</form>
		</div>
	</div>
</div>
</div>
@endsection
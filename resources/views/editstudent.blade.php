@extends('templates.template')
@section('title','Edit Student')
@section('content')

<div style="background-image: url(/images/students.jpg); background-size:cover; background-attachment: fixed; color: #0984e3; font-weight: bolder; text-shadow: 1px 1px #fff;">
<h1 class="text-center py-5">Edit Student Results</h1>
	
<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/admin/editstudent/{{$student->id}}" method="POST">
				@csrf
				{{method_field("PATCH")}}
				<div class="form-group">
					<label for="date">Exam Date:</label>
					<input type="date" name="date" class="form-control" value="{{$student->date}}">
				</div>				
				<div class="form-group">
					<label for="firstName">Firstname:</label>
					<input type="text" name="firstName" class="form-control" value="{{$student->firstName}}">
				</div>
				<div class="form-group">
					<label for="lastName">Lastname:</label>
					<input type="text" name="lastName" class="form-control" value="{{$student->lastName}}">
				</div>
				<div class="form-group">
					<label for="batch">Batch:</label>
					<select name="batch_id" class="form-control">
						@foreach($batches as $batch)
						<option value="{{$batch->id}}" {{$batch->id == $student->batch_id ? "selected" : ""}}>{{$batch->name}}</option>
						@endforeach
					</select>
					
				</div>
				<div class="form-group">
					<label for="subject">Subject:</label>
					<select name="subject_id" class="form-control">
						@foreach($subjects as $subject)
						<option value="{{$subject->id}}"{{$subject->id == $student->subject_id ? "selected" : ""}}>{{$subject->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="score">Exam Score:</label>
					<input type="number" name="score" class="form-control" value="{{$student->score}}">
				</div>
				<div class="form-group">
					<label for="marks">Mark:</label>
					<select name="mark_id" class="form-control">
						@foreach($marks as $mark)
						<option value="{{$mark->id}}"{{$mark->id == $student->mark_id ? "selected" : ""}}>{{$mark->name}}</option>
						@endforeach
					</select>
				</div>
				
				<button type="submit" class="btn btn-success">Update</button>
			</form>
		</div>
	</div>
</div>
</div>
@endsection
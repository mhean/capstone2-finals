@extends('templates.template')
@section('title','Online Results System')
@section('content')
    
    <div class="banner">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Online Results System
                </div>                  
                <div class="signup-button" data-aos="fade-up">
                    <a href="/register">Sign Up</a>
                </div>                    
            </div>
        </div>
    </div>
    
@endsection
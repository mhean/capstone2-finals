@extends('templates.template')
@section('title','Students Results')
@section('content')

<div class="student-record">
	<h1 class="text-center py-5">Student Records</h1>
		@if(Session::has("message"))
			<h4 class="text-center alert alert-success">{{Session::get('message')}}</h4>
		@endif

	<!-- ADD student MODAL -->
		<div class="modal fade" id="addstudent">
				<div class="modal-dialog " role="document">
					<div class="modal-content" style="background-color: #dfe6e9">
				      <div class="modal-header" style="background-color: #b2bec3">
						<h5 class="modal-title">Add New Records</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
								
					<form action="/studentrecord" method="POST">
					@csrf
					<div class="modal-body">
						<div class="form-group">
							<label for="date">Exam Date:</label>
							<input type="date" name="date" class="form-control">
						</div>
						
						<div class="form-group">
							<label for="firstName">Firstname:</label>
							<input type="text" name="firstName" class="form-control">
						</div>
						<div class="form-group">
							<label for="lastName">Lastname:</label>
							<input type="text" name="lastName" class="form-control">
						</div>
						<div class="form-group">
							<label for="batch">Batch:</label>
							<select name="batch_id" class="form-control">
								@foreach($batches as $batch)
								<option value="{{$batch->id}}">{{$batch->name}}</option>
								@endforeach
							</select>
							
						</div>
						<div class="form-group">
							<label for="subject">Subject:</label>
							<select name="subject_id" class="form-control">
								@foreach($subjects as $subject)
								<option value="{{$subject->id}}">{{$subject->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="score">Exam Score:</label>
							<input type="number" name="score" class="form-control">
						</div>
						<div class="form-group">
							<label for="marks">Mark:</label>
							<select name="mark_id" class="form-control">
								@foreach($marks  as $mark)
								<option value="{{$mark->id}}">{{$mark->name}}</option>
								@endforeach
							</select>
						</div>
						
						<button type="submit" class="btn btn-success">Add</button>
				
					</div>
					</form>
					</div>
				</div>
			</div>
	<!-- END ADD SCHEDULE MODAL -->

	<div class="container">
			
		<div class="col-lg-2 form-group" >
			<!-- <h5>Filter by Batch:</h5> -->
				<form action="/studentfilter" method="POST">	
					@csrf			
					<select name="batch_id" class="form-control">
						]<option value="0">All Batch</option>
						<optgroup label="Batch">
							@foreach($batches as $batch)
							<option value="{{$batch->id}}"
							@if(isset($batchId))
								@if($batchId==$batch->id)selected
								@endif
							@endif
							>{{$batch->name}}</option>
						@endforeach
						</optgroup>
					</select>			
					
					<button type="submit" class="btn btn-success my-2">Filter</button>				
				</form>	
				@auth
				@if(Auth::user()->role_id == 1)
				<div >
					<a data-toggle="modal" data-target="#addstudent" class="btn btn-add">Add Records </a>			
				</div>
				@endif
				@endauth
		</div>
		<div class="row records">
			<div class="col-lg-10 offset-lg-1">
				<table class="table table-striped">
					<thead>
						<tr class="thead">
							<th style="width:150px">Exam Date</th>
							<th style="width:150px">Student ID</th>
							<th style="width:180px">Student Name</th>
							<th>Batch</th>
							<th>Subject</th>
							<th>Results</th>		
							<th>Marks</th>
							<th>Updated</th>			
							
							<th>Action</th>	
									
						</tr>
					</thead>
					<tbody>
						
						@foreach($students as $student)
						<tr class="tbody">
							<td>{{$student->date}}</td>
							<td>0001-00{{$student->id}}</td>
							<td>{{$student->firstName." ".$student->lastName}}</td>
							<td>{{$student->batch->name}}</td>
							<td>{{$student->subject->name}}</td>
							<td>{{$student->score}}</td>
							<td>{{$student->mark->name}}</td>
							<td>{{$student->created_at->diffForHumans()}}</td>
							<td>
								<a href="/indivrecord/{{$student->id}}" class="btn btn-secondary form-control" ><i class="fa fa-eye pt-1"></i></a>
								@auth
								@if(Auth::user()->role_id == 1)
								<a href="/admin/editstudent/{{$student->id}}" class="btn btn-info form-control"><i class="fa fa-edit" style="font-size:20px;color:#fff;"></i></a>
								<form class="delete_form" action="/admin/removestudent/{{$student->id}}" method="POST">
									@csrf
									{{method_field("DELETE")}}
									<button type="submit" class="btn btn-danger form-control"><i class="fa fa-remove" style="font-size:20px;color:#fff;"></i></button>
								</form>
								@endif
								@endauth
							</td>
						</tr>
						@endforeach
						
					</tbody>
				</table>
				
			</div>		
				
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_form').on('submit',function(){
			if(confirm("Are you sure you want to delete it?"))
			{
				return true;
			}else{
				return false;
			}
		});
	});
</script>

@endsection
@extends('templates.template')
@section('title','View Records')
@section('content')

<div style="background-image: url(/images/students.jpg); background-size:cover; background-attachment: fixed;  height: 100vh">
<h1 class="text-center py-5" style="color: #74b9ff; text-shadow: 1px 1px #fff">View Records</h1>

<div class="container ">
	<div class="row">
		<div class="col-lg-4 offset-lg-4" >
			<div class="card p-3" style="background: rgba(0,0,0,0.8); color: #fff">
				<p class="card-title" style="font-size:25px"><strong style="color: #74b9ff">Student Name:</strong> {{$students->firstName. " " . $students->lastName}}</p>
				<p class="card-text" style="font-size:25px"><strong style="color: #74b9ff">Student Id:</strong> 0001-00{{$students->id}}</p>
				<p class="card-text" style="font-size:25px"><strong style="color: #74b9ff">Subject:</strong> {{$students->subject->name}}</p>
				<p class="card-text" style="font-size:25px"><strong style="color: #74b9ff">Batch:</strong> {{$students->batch->name}}</p>
				<p class="card-text" style="font-size:25px"><strong style="color: #74b9ff">Exam Result:</strong> {{$students->score}}</p>
				<p class="card-text" style="font-size:25px"><strong style="color: #74b9ff">Mark:</strong> {{$students->mark->name}}</p>

				@auth
				@if(Auth::user()->role_id == 1)
				<a href="/admin/editstudent/{{$students->id}}" class="btn btn-info form-control"><i class="fa fa-edit" style="font-size:20px;color:#fff;"></i>Edit</a><br>
				<form class="delete_form" action="/admin/removestudent/{{$students->id}}" method="POST">
					@csrf
					{{method_field("DELETE")}}
					<button type="submit" class="btn btn-danger form-control"><i class="fa fa-remove" style="font-size:20px;color:#fff;"></i>Delete</button>
				</form>
				@endif
				@endauth
			</div>
		</div>
			
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_form').on('submit',function(){
			if(confirm("Are you sure you want to delete it?"))
			{
				return true;
			}else{
				return false;
			}
		});
	});
</script>

@endsection
@extends('templates.template')
@section('title','Students Requests')
@section('content')

<h1 class="text-center py-5">Student Requests</h1>
	@if(Session::has("message"))
		<h4 class="text-center alert alert-success">{{Session::get('message')}}</h4>
	@endif

<div class="container ">
	@auth
	@if(Auth::user()->role_id == 2)
	<div class="offset-lg-1">
		<a  class="btn btn-primary"><i class="fas fa-user-plus"></i>Add Request </a>	
	</div>	
	@endif
	@endauth
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Student Name</th>
						<th>Batch</th>
						<th>Subject</th>	
						<th>Email</th>			
						<th>Request/Comment</th>			
						@auth
							@if(Auth::user()->role_id == 2)
							<th>Action</th>				
							@endif
							@endauth
					</tr>
				</thead>
				<tbody>
					@foreach($student_requests as $student_request)
					<tr>
						<td>{{$student_request->student->firstName}}{{$student_request->student->lastName}}</td>
						<td>{{$student_request->batch->name}}</td>
						<td>{{$student_request->subject->name}}</td>
						<td>{{$student_request->request}}</td>
						<td>{{$student_request->email}}</td>					
						<td>
							@auth
							@if(Auth::user()->role_id == 2)
							<a href="" class="btn btn-info form-control"><i class="fa fa-edit" style="font-size:20px;color:#fff;"></i></a>
							<form action="/studentrequest/delete/{{$student_request->id}}" method="POST">
								@csrf
								{{method_field("DELETE")}}
								<button type="submit" class="btn btn-danger form-control"><i class="fa fa-remove" style="font-size:20px;color:#fff;"></i></button>
							</form>

							@endif
							@auth
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
			
		</div>
	</div>
</div>

@endsection
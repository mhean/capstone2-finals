<?php
namespace App\Http\Controllers;

use App\Student;
use App\Batch;
use App\Mark;
use App\Subject;
use Session;


use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->batch_id) && $request->batch_id>0){
            $subjects = Subject::all();
            $marks = Mark::all();
            $batches= Batch::all();
            $students= Student::where('batch_id', $request->batch_id)->get();
            $batchId = $request->batch_id;

        // dd($batchId);
            return view('/studentrecord',compact('students','batches','marks','subjects','batchId'));

        }else{
            $subjects= Subject::all();
            $marks= Mark::all();
            $students= Student::all();
            $batches=Batch::all();

            return view('/studentrecord',compact('subjects','marks','students','batches'));           
        }

        
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students= Student::all();
        $batches= Batch::all();
        $subjects= Subject::all();
        $marks= Mark::all();
        
        
        return view('studentrecord', compact('students','batches','subjects','marks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=array(
            "date"=>"required",
            "firstName"=>"required",
            "lastName"=>"required",
            "batch_id"=>"required",
            "score"=>"required",
            "subject_id"=>"required",
            "mark_id"=>"required"
        );
        $this->validate($request, $rules);

        $new_student = new Student;
        $new_student->date=$request->date;
        $new_student->firstName = $request->firstName;
        $new_student->lastName = $request->lastName;
        $new_student->batch_id = $request->batch_id;
        $new_student->score = $request->score;
        $new_student->subject_id = $request->subject_id;
        $new_student->mark_id = $request->mark_id;
        $new_student->save();

        Session::flash("message","New record has been added!");

        return redirect('/studentrecord');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $students = Student::find($id);
        $batches=Batch::all();
        $subjects=Subject::all();
        $marks=Mark::all();

        return view('indivrecord',compact('students','batches','subjects','marks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        
        $batches=Batch::all();
        $subjects=Subject::all();
        $marks=Mark::all();

        return view('editstudent',compact('student','batches','subjects','marks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $student=Student::find($id);

        $rules=array(
            "date"=>"required",
            "firstName"=>"required",
            "lastName"=>"required",
            "batch_id"=>"required",
            "score"=>"required",
            "subject_id"=>"required",
            "mark_id"=>"required"
        );

        $this->validate($request, $rules);

        $student =Student::find($id);
        $student->date=$request->date;
        $student->firstName = $request->firstName;
        $student->lastName = $request->lastName;
        $student->batch_id = $request->batch_id;
        $student->score = $request->score;
        $student->subject_id = $request->subject_id;
        $student->mark_id = $request->mark_id;

    
        $student->save();
        Session::flash("message"," $student->firstName $student->lastName has been updated!");
        return redirect('/studentrecord');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function delete($id)

    {   $student= Student::find($id);
        $studentToRemove=Student::find($id);
        $studentToRemove->delete();
        Session::flash("message"," $student->firstName $student->lastName successfully deleted!");
        return redirect('/studentrecord');
    }
    
    // public function indivrecord($id){
    //     $student=Student::find($id);
    //     $subject=Subject::all();
    //     $batch=Batch::all();
    //     $mark=Mark::all();
    //     return view('indivrecord', compact ('student','subject','batch','mark'));
    // }
}

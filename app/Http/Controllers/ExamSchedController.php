<?php

namespace App\Http\Controllers;

use App\Exam_sched;
use App\Subject;
use App\Batch;
use Session;
use Illuminate\Http\Request;

class ExamSchedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exam_scheds= Exam_sched::sortable()->paginate(3);
        return view('examschedule',compact('exam_scheds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exam_scheds=Exam_sched::all();
        $subjects=Subject::all();
        $batches=Batch::all();
        return view('examschedule', compact('exam_scheds','subjects','batches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= array(
            "date"=>"required",
            "subject_id"=>"required",
            "batch_id"=>"required",
            "roomNo"=>"required",
            "startTime"=>"required",
            "endTime"=>"required"
        );
        $this->validate($request, $rules);

        $exam_sched= new Exam_sched;

        $exam_sched->date=$request->date;
        $exam_sched->subject_id=$request->subject_id;
        $exam_sched->batch_id=$request->batch_id;
        $exam_sched->roomNo=$request->roomNo;
        $exam_sched->startTime=$request->startTime;
        $exam_sched->endTime=$request->endTime;

        $exam_sched->save();

        Session::flash("message","New Schedule has been added");

        return redirect('/examschedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam_sched  $exam_sched
     * @return \Illuminate\Http\Response
     */
    public function show(Exam_sched $exam_sched)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam_sched  $exam_sched
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
 // public function edit()
    {
       
        $exam_sched = Exam_sched::find($id);
        $subjects=Subject::all();
        $batches=Batch::all();

        return view('editschedule',compact('exam_sched','subjects','batches'));
        // return view('examschedule');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam_sched  $exam_sched
     * @return \Illuminate\Http\Response
     */
     // public function update(Request $request, Exam_sched $exam_sched)
    public function update(Request $request, $id)
    {
        $exam_sched= Exam_sched::find($id);

        $rules= array(
            "date"=>"required",
            "subject_id"=>"required",
            "batch_id"=>"required",
            "roomNo"=>"required",
            "startTime"=>"required",
            "endTime"=>"required"
        );

        $this->validate($request, $rules);

        $exam_sched= Exam_sched::find($id);
        $exam_sched->date=$request->date;
        $exam_sched->batch_id=$request->batch_id;
        $exam_sched->subject_id=$request->subject_id;
        $exam_sched->roomNo=$request->roomNo;
        $exam_sched->startTime=$request->startTime;
        $exam_sched->endTime=$request->endTime;

        $exam_sched->save();

        Session::flash("message","Schedule has been updated!");

        return redirect('/examschedule');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam_sched  $exam_sched
     * @return \Illuminate\Http\Response
     */
    public function delete($id)

    {   $exam_sched= Exam_sched::find($id);
        $schedToRemove=Exam_sched::find($id);
        $schedToRemove->delete();
        // Session::flash("message","Successfully Deleted!");
        return redirect('/examschedule')->with('success','Data Deleted');
    }
}

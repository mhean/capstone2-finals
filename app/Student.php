<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Student extends Model
{
    use Sortable;
    protected $fillable = [ 'firstName', 'lastName', 'date', 'score', 'batch_id', 'subject_id', 'mark_id'];


    public $sortable = ['id', 'firstName', 'lastName', 'date', 'score', 'batch_id', 'subject_id', 'mark_id', 'created_at'];
    
    public function batch(){
    	return $this->belongsTo('\App\Batch','batch_id');
    }
     public function subject(){
    	return $this->belongsTo('\App\Subject','subject_id');
    }
     public function mark(){
    	return $this->belongsTo('\App\Mark','mark_id');
    }
    public function role(){
    	return $this->belongsTo('\App\Role','role_id');
	}

}

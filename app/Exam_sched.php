<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Exam_sched extends Model
{
	use Sortable;

    protected $fillable = ['date', 'startTime', 'endTime', 'roomNo', 'batch_id', 'subject_id'];

    public $sortable = [
        'id', 'date', 'startTime', 'endTime', 'roomNo', 'batch_id', 'subject_id', 'created_at'
    ];

     public function subject(){
      return $this->belongsTo('\App\Subject','subject_id');
    }
    public function batch(){
      return $this->belongsTo('\App\Batch','batch_id');
    }
}
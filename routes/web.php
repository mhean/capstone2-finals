<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});

Auth::routes();

// adding student
Route::get('/studentrecord', 'StudentController@index')->name('student');
// Route::get('/studentrecord/{id}', 'StudentController@indivrecord');
//filtering
Route::post('/studentfilter','StudentController@index');
//Sorting
// Route::get('/studentrecord', 'StudentController@sort');

// 
Route::get('/studentrecord', 'StudentController@create');
Route::post('/studentrecord','StudentController@store');
// edit student
Route::get('/admin/editstudent/{id}', 'StudentController@edit');
//show
Route::get('/indivrecord/{id}', 'StudentController@show');
//update
Route::patch('/admin/editstudent/{id}','StudentController@update');
// remove/deleting 
Route::delete('/admin/removestudent/{id}','StudentController@delete');



//EXAM SCHEDULE
	//adding
	Route::get('/examschedule', 'ExamSchedController@index');
	Route::get('/examschedule', 'ExamSchedController@create');
	Route::post('/examschedule', 'ExamSchedController@store');
	//editing
	Route::get('/examschedule/{id}', 'ExamSchedController@edit');
	Route::patch('/examschedule/{id}', 'ExamSchedController@update');
	//deleting
	Route::delete('/examschedule/{id}', 'ExamSchedController@delete');
	//viewing data

//USER CONTROLLER
	Route::get('/users','UserController@index');


//STUDENT REQUEST CONTROLLER//
	//create CRUD
	Route::get('/studentrequest', 'StudentRequestController@index');
	Route::get('/studentrequest', 'StudentRequestController@create');
	Route::post('/studentrequest', 'StudentRequestController@store');

	Route::delete('/studentrequest/delete/{id}', 'StudentRequestController@delete');
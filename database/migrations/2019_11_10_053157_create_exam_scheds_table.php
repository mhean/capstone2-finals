<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamSchedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_scheds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('startTime');
            $table->string('endTime');
            $table->string('roomNo');
            $table->unsignedBigInteger('batch_id');
            $table->unsignedBigInteger('subject_id');

            $table->timestamps();
            $table->foreign('subject_id')
            ->on('subjects')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('batch_id')
            ->on('batches')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_scheds');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('date');
            $table->unsignedBigInteger('score');            
            $table->unsignedBigInteger('batch_id');
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('mark_id');
            $table->timestamps();

            

            $table->foreign('batch_id')
            ->on('batches')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('subject_id')
            ->on('subjects')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('mark_id')
            ->on('marks')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
